<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Register Daftar Akun
Route::get('/daftar','DaftarController@index');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/projects', function () {
    return view('layouts.projectcontent');
});

// Route::get('/buatproyek', function () {
//     return view('layouts.createprojects');
// });

Route::get('/projects','CreateProjectController@index');

Route::get('/buatproyek','ProjectController@indexcategory');
Route::post('/buatproyek','ProjectController@store');


Route::get('/detailproyek', function () {
    return view('layouts.detailproyek');
});
