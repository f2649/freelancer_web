<nav class="navbar navbar-light navbar-expand-md bg-light justify-content-md-center justify-content-start m-b-0">
    <a class="navbar-brand d-md-none d-inline" href="">Brand</a>
    <button class="navbar-toggler ml-1" type="button" data-toggle="collapse" data-target="#collapsingNavbar2">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="nav-link" href="#_"><i class="fa fa-search mr-1"></i></a>
    <div class="navbar-collapse collapse justify-content-between align-items-center w-100" id="collapsingNavbar2">
        <ul class="navbar-nav mx-auto text-md-center text-left">
            <li class="nav-item">
                <a class="nav-link" href="#">Beranda</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Project</a>
            </li>
            <li class="nav-item my-auto">
                <a class="nav-link navbar-brand mx-0 d-none d-md-inline" href="">Dari Rumah</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Apa lagi</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Bingung</a>
            </li>
        </ul>
        <ul class="nav navbar-nav flex-row justify-content-md-center justify-content-start flex-nowrap">
            <li class="nav-item"><a class="nav-link" href=""><i class="fa fa-facebook mr-1"></i></a> </li>
            <li class="nav-item"><a class="nav-link" href=""><i class="fa fa-twitter"></i></a> </li>
        </ul>
    </div>
</nav>
