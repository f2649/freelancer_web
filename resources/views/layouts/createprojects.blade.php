@extends('layouts.master')

@section('title')
    Create Projects
@endsection

@section('content')
<br>
    <div class="container mb-5">
        <form action="/projects" method="POST">
            @csrf
        <h3>Buat Proyek</h3>
        <hr>
        <div class="row">
            <div class="col-md-6">
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" placeholder="" name="judul" id="judul">
            </div>
            </div>
            <!--  col-md-6   -->
            <div class="col-md-6">
            <div class="form-group">
                <label for="budget">Budget</label>
                <input type="number" class="form-control" placeholder="" name="budget" id="budget">
            </div>
            </div>
            <!--  col-md-6   -->
        </div>
        <br>
        <label class="col-xs-3 control-label">Deskripsi</label>
        <div class="col-xs-9">
            <textarea class="form-control" name="deskripsi" id="exampleFormControlTextarea1" rows="8"></textarea>
        </div>
        <br>

        <div class="row">
            <div class="col-md-6">
            <div class="form-group">
                <label for="company">Hari selesai</label>
                <input type="number" name="selesai" class="form-control" placeholder="" id="company">
            </div>
            </div>
            <!--  col-md-6   -->


            <div class="col-md-6">
            <div class="form-group">
                <label class="" for="customFile">Berkas pendukung</label>
                <input type="file" class="form-control" id="customFile" />
            </div>
            </div>
            <!--  col-md-6   -->
        </div>
        <!--  row   -->

        <br>

        @foreach($category as $category)
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="kategori" value="{{ $category->nama_kategori }}" />
            <label class="form-check-label" for="inlineCheckbox1">{{ $category->nama_kategori}}</label>
        </div>
        @endforeach

        <br><br>
        <label for="persetujuan"><b>Degan ini Anda telah menyetujui ketentuan bla bla bla</b></label>
        <div class="checkbox">
            <label>
            <input class="form-check-input" type="checkbox" name="setuju" value="ya" id="persetujuan">
            <label class="form-check-label" for="persetujuan">Ya, Saya setuju.</label>
            </label>
        </div>
        <br>

        <a type="submit" class="btn btn-primary">Submit</a>
        </form>
    </div>

@endsection
