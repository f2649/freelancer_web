
@extends('layouts.master')

@section('title')
    Daftar
@endsection

@section('content')
<div class="container">

<section class="panel panel-default">
  <div class="panel-heading">
  <h3 class="panel-title">Daftar akun</h3>
  </div>
  <div class="panel-body">

  <form action="designer-finish.html" class="form-horizontal" role="form">
   <!-- form-group // -->
     <div class="form-group">
      <label for="name" class="col-sm-3 control-label">Username</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" name="username" id="username" placeholder="Username">
      </div>
    </div> <!-- form-group // -->
    <div class="form-group">
      <label for="name" class="col-sm-3 control-label">Nama Lengkap</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" name="name" id="name" placeholder="Nama Lengkap">
      </div>
    </div> <!-- form-group // -->
    <div class="form-group">
      <label for="about" class="col-sm-3 control-label">Bio</label>
      <div class="col-sm-9">
        <textarea class="form-control"></textarea>
      </div>
    </div> <!-- form-group // -->
    <div class="form-group">
      <label for="qty" class="col-sm-3 control-label">Email</label>
      <div class="col-sm-3">
     <input type="email" class="form-control" name="qty" id="qty" placeholder="Email">
      </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-3 control-label">Tingkat keahlian</label>
        <div class="col-sm-9">
            <label class="radio-inline">
          <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Внешный заказ
        </label>
        <label class="radio-inline">
          <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Внутренный заказ
        </label>
        </div>
    </div>
     <!-- form-group // -->
    <div class="form-group">
      <label for="name" class="col-sm-3 control-label">Foto Profil</label>
      <div class="col-sm-3">
        <label class="control-label small" for="file_img">Foto Profil (jpg/png):</label> <input type="file" name="file_img">
      </div>
      <div class="col-sm-3">
        <label class="control-label small" for="file_img">Portofolio</label>  <input type="file" name="file_archive">
      </div>
    </div> <!-- form-group //
    <div class="form-group">
      <label for="tech" class="col-sm-3 control-label">Технолог</label>
      <div class="col-sm-3">
     <select class="form-control">
      <option value="">Выберите</option>
      <option value="texnolog2">Технолог 2</option>
      <option value="texnolog3">Технолог 3</option>
     </select>
      </div>
    </div> -->
    <hr>
    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-primary">Отправить</button>
      </div>
    </div> <!-- form-group // -->
  </form>

  </div><!-- panel-body // -->
  </section><!-- panel// -->


  </div> <!-- container// -->
@endsection
