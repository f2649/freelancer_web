
@extends('layouts.master')

@section('title')
    Projects
@endsection

@section('content')
    <div class="jumbotron text-center text-white m-t-0" style="background-image: url('jt_welcome2.jpg');height: 250px;">
        <div class="mask" style="background-color: rgba(0,0,0,0.6);height:250px;">
            <div class="d-flex justify-content-center align-items-center h-100 ">
                <div>
                    <p class="lead">Punya banyak proyek tapi bingung tak ada yang ngerjain ?</p>
                    <hr class="my-4">
                    <p>Posting proyekmu sekarang dan dapatkan talenta yang sesuai !</p>
                    <a class="btn btn-outline-light btn" href="#" role="button">Buat Proyek</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-5 ms-3 mr-3">
        <div class="col-4">
            <div class="row container-fluid">
                <h5><b>Kategori</b></h5>
            <div class="list-group me-5 mt-2 mb-5" id="list-tab" role="tablist">
                @foreach($category as $category)
                <a class="list-group-item list-group-item-action" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">{{ $category->nama_kategori }}</a>
                @endforeach
            </div>
        </div>
     </div>

        <div class="col-8">
            <div class="row container-fluid">
                <div class="col col-lg-10">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                  </div>
                  <div class="col col-lg-1 mb-4">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                  </div>
                <h5 class="pe-3">Mengerjakan sistem informasi puskesmas android menggunakan kotlin dan database firebase realtime database</h5>
                <div>
                    <div class="bd-highlight"><b>Tanggal posting : </b>20/02/2022</p></div>
                </div>
                <p>
                    The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog
                    The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog
                    The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog
                    The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog
                    The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog
                    The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog
                </p>
                <div>
                    <div>
                        <div class="row">
                            <div class="d-flex bd-highlight mt-1 pe-5">
                                <div class="bd-highlight">
                                    <p>
                                        <b>Status : </b>Aktif merekrut <br>
                                        <b>Budget : </b>Rp.1.000.000 <br>
                                        <b>Owner : </b>Kartiko Johar <br>
                                        <b>Deadline : </b>30/10/2022
                                </div>

                                <div class="ms-auto bd-highlight">
                                    <a href="" class="btn btn-outline-secondary btn-sm mb-2">Grapich Design</a>
                                    <a href="" class="btn btn-outline-secondary btn-sm mb-2">Illustration</a>
                                    <a href="" class="btn btn-outline-secondary btn-sm mb-2">Animation</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="mt-1 mb-4">


                <h5 class="pe-3">Mengerjakan sistem informasi puskesmas android menggunakan kotlin dan database firebase realtime database</h5>
                <div>
                    <div class="bd-highlight"><b>Tanggal posting : </b>20/02/2022</p></div>
                </div>
                <p>
                    The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog
                    The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog
                    The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog
                    The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog
                    The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog
                    The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog
                </p>
                <div>
                    <div>
                        <div class="row">
                            <div class="d-flex bd-highlight mt-1 pe-5">
                                <div class="bd-highlight">
                                    <p>
                                        <b>Status : </b>Aktif merekrut <br>
                                        <b>Budget : </b>Rp.1.000.000 <br>
                                        <b>Owner : </b>Kartiko Johar <br>
                                        <b>Deadline : </b>30/10/2022
                                </div>

                                <div class="ms-auto bd-highlight">
                                    <a href="" class="btn btn-outline-secondary btn-sm mb-2">Grapich Design</a>
                                    <a href="" class="btn btn-outline-secondary btn-sm mb-2">Illustration</a>
                                    <a href="" class="btn btn-outline-secondary btn-sm mb-2">Animation</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="mt-1 mb-4">
            </div>
        </div>
      </div>
@endsection

