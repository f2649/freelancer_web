<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectCategory extends Model
{
    protected $table = "projectcategory";
    protected $fillable = ["id", "nama_kategori"];
}
